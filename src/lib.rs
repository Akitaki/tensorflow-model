//! Safe (or at least safer) wrapper around using Tensorflow's SavedModel format from Rust.

extern crate tensorflow as tf;

use std::collections::HashMap;
use strum_macros::Display;

/// An already-loaded tensorflow model, containing a living session.
///
/// ```no_run
/// let mut model = Model::load_from_path("./my_model")?;
/// {
///     let mut operation = model.operate();
///     let token = operation.add_fetch_target("my_function_sig", "my_output");
///     operation.run()?;
///     let output: tf::Tensor<f32> = operation.fetch(token);
///     let output: &[_] = &output;
///     println!("The output from graph function is {:?}", output);
/// }
/// ```
#[derive(Debug)]
pub struct Model {
    session: tf::Session,
    graph: tf::Graph,
    /// The function signatures from the MetaGraphDef, for looking up inputs
    /// and outputs from signature name and variable names.
    signatures: HashMap<String, tf::SignatureDef>,
}

impl Model {
    /// Load a new session from directory at the specified path.  The model
    /// should be in the SavedModel format, which can be created by using
    /// the `tf.saved_model.save()`.
    pub fn load_from_path<S: AsRef<str>>(path: S) -> Result<Self, Error> {
        let mut graph = tf::Graph::new();
        let bundle = tf::SavedModelBundle::load(
            &tf::SessionOptions::new(),
            &["serve"],
            &mut graph,
            path.as_ref(),
        )
        .map_err(|_| Error::BundleLoad)?;

        let signatures: HashMap<String, tf::SignatureDef> =
            bundle.meta_graph_def().signatures().clone();
        let tf::SavedModelBundle { session, .. } = bundle;

        Ok(Self {
            session,
            graph,
            signatures,
        })
    }

    /// Show name, inputs, and outputs for each function signature read from
    /// the MetaGraphDef.
    pub fn print_signatures(&self) {
        for (name, sig) in self.signatures.iter() {
            println!("{}: {{", name);
            println!("    name: {},", sig.method_name());
            println!("    inputs: {{");
            for (name, tensor_info) in sig.inputs().iter() {
                println!("        {}: {:?}", name, tensor_info);
            }
            println!("    }},");
            println!("    outputs: {{");
            for (name, tensor_info) in sig.outputs().iter() {
                println!("        {}: {:?}", name, tensor_info);
            }
            println!("    }},");
            println!("}}");
        }
    }

    /// Create an operation bound to this model.  `&mut self` is chosen to
    /// avoid race conditions as per documentation of `Session::run`.
    pub fn operate<'a>(&'a mut self) -> ModelOperation<'a> {
        ModelOperation {
            model: self,
            run_args: tf::SessionRunArgs::new(),
        }
    }
}

/// An operation that's either pending or already run.
pub struct ModelOperation<'a> {
    model: &'a mut Model,
    run_args: tf::SessionRunArgs<'a>,
}

#[derive(Debug)]
enum ModelIOKind {
    Input,
    Output,
}

impl<'a> ModelOperation<'a> {
    /// Get `tf::Operation` instance from function signature and name.
    fn get_operation_from(
        &self,
        sig_name: &str,
        io_name: &str,
        op_kind: ModelIOKind,
    ) -> tf::Operation {
        let ref signature = self.model.signatures[sig_name];
        let ios = match op_kind {
            ModelIOKind::Input => signature.inputs(),
            ModelIOKind::Output => signature.outputs(),
        };
        let ref op_name = ios[io_name].name().name;
        let op = self
            .model
            .graph
            .operation_by_name_required(op_name)
            .expect(&format!(
                "Failed to get input/output/target '{}' from signature '{}'",
                sig_name, io_name
            ));
        op
    }

    /// Add input to the operation.
    pub fn add_feed<T: tf::TensorType, U: AsRef<str>, V: AsRef<str>>(
        &mut self,
        sig_name: U,
        input_name: V,
        tensor: &'a tf::Tensor<T>,
    ) {
        let op =
            self.get_operation_from(sig_name.as_ref(), input_name.as_ref(), ModelIOKind::Input);
        self.run_args.add_feed(&op, 0, tensor);
    }

    /// Add running target (without fetching returned value) to the operation.
    pub fn add_target<U: AsRef<str>, V: AsRef<str>>(&mut self, sig_name: U, target_name: V) {
        let op =
            self.get_operation_from(sig_name.as_ref(), target_name.as_ref(), ModelIOKind::Output);
        self.run_args.add_target(&op);
    }

    /// Add running target with fetching request to the operation.
    pub fn add_fetch_target<U: AsRef<str>, V: AsRef<str>>(
        &mut self,
        sig_name: U,
        output_name: V,
    ) -> tf::FetchToken {
        let op =
            self.get_operation_from(sig_name.as_ref(), output_name.as_ref(), ModelIOKind::Output);
        self.run_args.request_fetch(&op, 0)
    }

    /// Run the graph operations.
    pub fn run(&mut self) -> Result<(), Error> {
        Ok(self
            .model
            .session
            .run(&mut self.run_args)
            .map_err(|e| Error::OperationFail(format!("{:?}", e)))?)
    }

    /// Fetch required targets using a token.  This must be called after
    /// calling [`ModelOperation::run()`].
    pub fn fetch<T: tf::TensorType>(
        &mut self,
        token: tf::FetchToken,
    ) -> Result<tf::Tensor<T>, Error> {
        Ok(self
            .run_args
            .fetch(token)
            .map_err(|e| Error::FetchFail(format!("{:?}", e)))?)
    }
}

#[derive(Debug, Display)]
pub enum Error {
    BundleLoad,
    OperationFail(String),
    FetchFail(String),
}

impl std::error::Error for Error {}
